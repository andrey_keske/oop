/**
 * Top Image
 *
 * Insert source from `img` to `.top-image`
 */

!(function() {

  var CLASS = '.top-image';
  var CURRENT_SLIDE = 0;

  var AUTO = true,
    SPEED = 7000;

  var topImage = {

    showPhotoInformation: function(action) {
      "use strict";

      $(CLASS).find('.slide .container').fadeOut(333);
      $(CLASS).find('.slide .container').eq(CURRENT_SLIDE).fadeIn(777);
    },

    move: function(action) {
      "use strict";

      var slides = $(CLASS).find('.slide').length,
        width = $(window).width();

      if (action === 'left') {
        // move to the left
        if (CURRENT_SLIDE < (slides - 1)) {
          CURRENT_SLIDE += 1;
          this.animate(width * CURRENT_SLIDE * -1);
        } else {
          CURRENT_SLIDE = 0;
          this.animate(0);
        }
      } else {
        // move to the right
        if (CURRENT_SLIDE > 0) {
          CURRENT_SLIDE -= 1;
          this.animate(width * CURRENT_SLIDE * -1);
        } else {
          CURRENT_SLIDE = (slides - 1);
          this.animate(width * (slides - 1) * -1);
        }
      }

      this.showPhotoInformation();
    },

    animate: function(width) {
      "use strict";

      // $(CLASS).find('.wrap').animate({
      //  marginLeft: width
      // }, 0);

      $(CLASS).find('.wrap').find('.slide').eq(CURRENT_SLIDE - 1).animate({
        opacity: 0
      }, 1111);
      $(CLASS).find('.wrap').find('.slide').eq(CURRENT_SLIDE).animate({
        opacity: 1
      }, 777);
    },

    resize: function() {
      "use strict";

      $(CLASS).find('.slide').css('width', $(window).width());
      $(CLASS).css('height', $(window).height() - 320);
      // this.showPhotoInformation();
    },

    autoAnimation: function() {
      "use strict";

      if (AUTO) {
        setTimeout(function() {
          topImage.move('left');
          topImage.autoAnimation();
        }, SPEED);
      } else {
        clearTimeout();
      }
    },

    init: (function() {
      $(document).ready(function() {
        topImage.resize();
        topImage.showPhotoInformation();

        topImage.autoAnimation();

        $(CLASS).find('.to-left').on('click', function() {
          topImage.move('right');
          AUTO = false;
        });

        $(CLASS).find('.to-right').on('click', function() {
          topImage.move('left');
          AUTO = false;
        });
      });

      $(window).resize(function() {
        topImage.resize();
      });
    }())
  }

}());

// Item card
+ (function() {

  var itemCard = {


    bind: (function() {
      'use strict';

      $(window).load(function() {
        $('.item-card-page .thumbs img').bind('click', function() {
          var src = $(this).attr('src');

          // Set image
          $('.item-card-page .photo img').attr('src', src);

          // Set class
          $('.item-card-page .thumbs img').removeClass('active');
          $(this).addClass('active');
        });

        $('.item-card-page .photo img').bind('click', function() {
          // var src = $(this).attr('src');

          // $('.item-card-page .photo img').each(function() {
          //   if ($(this).attr('src') === src) {
          //     console.log($(this).index())
          //   }
          // })
        });

        $('.zoom-foto').imagezoomsl({
          zoomrange: [3, 3]
        });
      })
    }())

  }

}());

// Journal page
+ (function() {

  var current = 0,
    $preview = $('#preview'),
    $carouselEl = $('#carousel'),
    $carouselItems = $carouselEl.children(),
    carousel = $carouselEl.elastislide({
      current: current,
      minItems: 4,
      onClick: function(el, pos, evt) {

        changeImage(el, pos);
        evt.preventDefault();

      },
      onReady: function() {

        changeImage($carouselItems.eq(current), current);

      }
    });

  function changeImage(el, pos) {

    $preview.attr('src', el.data('preview'));
    $carouselItems.removeClass('current-img');
    el.addClass('current-img');
    // carousel.setCurrent(pos);

  }

}());

$(function() {
  $('.scroll-pane').jScrollPane();
});
